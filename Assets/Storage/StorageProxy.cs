using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

[RequiresEntityConversion]
public class StorageProxy : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
{
    //public float3 target;

    public GameObject storageCellLook;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {




        var storage = new StorageComp { size = new int3(5, 1, 5), Model = conversionSystem.GetPrimaryEntity(storageCellLook) };
        dstManager.AddComponentData(entity, storage);
        dstManager.AddComponentData(entity, new Scale { Value = 0.4f });
        dstManager.AddBuffer<StorageCell>(entity);
        dstManager.AddBuffer<PosRequest>(entity);
        dstManager.AddComponentData(entity, new FreeStorage { });
        dstManager.AddComponentData(entity, new CreateStorageTag { });




    }

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
    {
        referencedPrefabs.Add(storageCellLook);
    }



}

public struct CreateStorageTag : IComponentData
{

}