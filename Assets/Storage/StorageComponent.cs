using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using Unity.Collections;
using System;

public struct StorageComp : IComponentData
{
    public int3 size;

    public int freeSlots;
    public Entity Model;

}

public struct StorageCell : IBufferElementData
{
    public Entity self;
    public Entity item;

}

public struct FreeStorage : IComponentData
{

}

/*
public struct StorageDirectory : ISharedComponentData, IEquatable<StorageDirectory>
{
    public NativeHashMap<int, NativeList<Entity>> resourceDirectory;

    public bool Equals(StorageDirectory other)
    {
        if (resourceDirectory.Equals(other.resourceDirectory))
        {
            return true;
        }
        return false;
    }

    public override int GetHashCode()
    {
        return resourceDirectory.GetHashCode();
    }
}
 */
