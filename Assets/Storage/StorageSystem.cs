using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;




public class StorageSystem : ComponentSystem
{





    protected override void OnCreate()
    {

    }
    // takes cell index and returns pos relative to storage pos
    // only 2d atm
    private float3 CalcPos(StorageComp storage, float3 pos, int i)
    {
        int xD = i % storage.size.x;
        int zD = i / storage.size.z;

        return new float3(pos.x + xD, pos.y, pos.z + zD);

    }

    private void FindEmptyCell(PosRequest request, DynamicBuffer<StorageCell> storageList, StorageComp storage, Translation storagePos, Entity entity)
    {
        var requesterB = World.Active.EntityManager.GetBuffer<PosResponse>(request.requester);
        PosResponse resp;
        for (int i = 0; i < storageList.Length; i++)
        {
            if (storageList[i].item == Entity.Null)
            {
                resp = new PosResponse { pos = CalcPos(storage, storagePos.Value, i), success = true, sender = entity };
                requesterB.Add(resp);
                return;
            }

        }

        if (storageList.Length < storage.size.x * storage.size.z)
        {
            resp = new PosResponse { pos = CalcPos(storage, storagePos.Value, storageList.Length), success = true, sender = entity };
            requesterB.Add(resp);
            return;

        }

        resp = new PosResponse { success = false };
        requesterB.Add(resp);
    }

    protected override void OnUpdate()
    {

        Entities.WithAllReadOnly<Translation, StorageComp, PosRequestTag>().ForEach(
            (Entity entity, ref Translation pos, ref StorageComp storage) =>
                {
                    Debug.Log("is executed");

                    // find first empty cell

                    var storageList = World.Active.EntityManager.GetBuffer<StorageCell>(entity);
                    var requests = World.Active.EntityManager.GetBuffer<PosRequest>(entity);

                    for (int r = 0; r < requests.Length; r++)
                    {
                        FindEmptyCell(requests[r], storageList, storage, pos, entity);
                    }

                    requests.Clear();
                    PostUpdateCommands.RemoveComponent<PosRequestTag>(entity);
                });


        Entities.WithAllReadOnly<Translation, StorageComp, CreateStorageTag>().ForEach(
                (Entity entity, ref Translation pos, ref StorageComp storage) =>
                {


                    var storageList = World.Active.EntityManager.GetBuffer<StorageCell>(entity);

                    Debug.Log(storage.size);

                    for (int x = 0; x < storage.size.x; x++)
                    {
                        for (int z = 0; z < storage.size.z; z++)
                        {
                            var cell = PostUpdateCommands.Instantiate(storage.Model);
                            var posCell = new float3(pos.Value.x + x * 2, pos.Value.y, pos.Value.z + z * 2);
                            PostUpdateCommands.SetComponent(cell, new Translation { Value = posCell });

                        }
                    }

                    PostUpdateCommands.RemoveComponent<CreateStorageTag>(entity);

                }

        );

    }

}