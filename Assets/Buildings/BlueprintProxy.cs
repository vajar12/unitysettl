﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class BlueprintProxy : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
{


    public GameObject model;



    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        var resourceList = dstManager.AddBuffer<BluePrintElement>(entity);


        var res = new BluePrintElement { quantity = 2, type = ResourceType.Wood };
        resourceList.Add(res);

        dstManager.AddComponentData<BluePrint>(entity, new BluePrint { Model = conversionSystem.GetPrimaryEntity(model), done = false });


    }

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
    {
        referencedPrefabs.Add(model);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
