using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Rendering;
using UnityEngine;
using Unity.Collections;


[InternalBufferCapacity(8)]
public struct ItemComp : IBufferElementData
{

    public Entity e;
}

[Serializable]
public struct InventoryComponent : IComponentData
{

    public int maxSize;
    public float weight;

}


public struct InteractionNotification : IComponentData
{

    public InteracitonType type;

    public Entity interactionObject;
    public Entity counterpart;


}


public enum InteracitonType
{
    SEND,
    RECEIVE
}


public struct ItemWorldTag : IComponentData
{

}

public struct NewItemWorldTag : IComponentData { }

