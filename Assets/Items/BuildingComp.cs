using System;
using Unity.Entities;



// this item can be used to build stuff
public struct Resource : IBufferElementData
{
    public Entity item;
    public ResourceType type;
    public float quality;
}



// if a blueprint has same amount of resources as blueprint elements it is built
public struct BluePrint : IComponentData
{
    public Entity Model;
    public bool done;
}




public struct BluePrintElement : IBufferElementData
{
    public ResourceType type;
    public int quantity;
}


public enum ResourceType
{
    Wood,
    Stone,
    Ice
}



public struct OpenBuildTask : IComponentData
{
    public ResourceType type;
    public Entity blueprint;

    public int quantity;
}


public struct OpenHaulTask : IComponentData
{
    public Entity item;

}