﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*

public delegate void removeItemToWorld(Item i, Vector3Int pos);
public delegate void addItemToWorld(Item i, Vector3Int pos);

public class ItemManager
{





    public worldObject[,,] World;
    public List<Item> items;
    public removeItemToWorld removeItemHandler;
    public addItemToWorld addItemHandler;



    public ItemManager(worldObject[,,] world, PlayerController pl)
    {
        pl.SendHandle += SendToWorld;
        pl.RecvHandle += RecvFromWorld;
        World = world;
    }


    public bool AddItem(Item i, Vector3Int pos)
    {
        if (checkAddItem(i, pos))
        {
            itemToWorld(i, pos);
        }
        return false;

    }

    private bool checkAddItem(Item i,Vector3Int pos)
    {
        var obj = World[pos.x, pos.y, pos.z];
   
        if(obj is IBlock==false){
            return false;
        }
        

        var block = obj as IBlock;
        return block.IsAir();


    }


    private void itemToWorld(Item i, Vector3Int pos)
    {
        //todo biogger items
        World[pos.x, pos.y, pos.z] = i;
        addItemHandler(i, pos);
    }
 

    //
    bool SendToWorld(Item i, Vector3Int pos, Inventory inv)
    {
        if (checkAddItem(i, pos) == false)
        {
            return false;
        }

        if (inv.TakeFromInventory(i) == false)
        {
            return false;
        }

        World[pos.x, pos.y, pos.z] = i;
        addItemHandler(i, pos);
        return true;



    }
    bool RecvFromWorld(Item i, Vector3Int pos, Inventory receiver)
    {
        if (receiver.SendToInventory(i)==false)
        {
            return false;
        }
        removeItemHandler(i, pos);
        return true;


    }

}
*/
