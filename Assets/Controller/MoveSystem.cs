﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class MoveSystem : JobComponentSystem
{
    public float validOffset = 0.1F;
    public BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    public struct MoveJob : IJobForEachWithEntity<MoveTask, Translation, SpeedComponent>
    {

        public EntityCommandBuffer.Concurrent CommandBuffer;
        [ReadOnly] public BufferFromEntity<PosRequest> posRequestsBuffer;
        public float DeltaTime;
        public float validOffset;

        public void Execute(Entity entity, int index, ref MoveTask mov, ref Translation pos, ref SpeedComponent speed)
        {
            if (mov.state == MoveTaskState.Done)
            {
                return;
            }

            var dist = math.distance(mov.target, pos.Value);
            if (dist < validOffset)
            {
                mov.state = MoveTaskState.Done;
                return;
            }

            // move now
            var dir = mov.target - pos.Value;
            dir = math.normalize(dir);

            pos.Value += speed.speed * DeltaTime * dir;

        }
    }

    protected override void OnCreate()
    {
        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }



    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var job = new MoveJob()
        {
            DeltaTime = Time.deltaTime,
            CommandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent(),
            validOffset = validOffset,
        };
        return job.Schedule(this, inputDeps);
    }
}