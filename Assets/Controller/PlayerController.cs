﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class PlayerController : MonoBehaviour
{

/** 
    
    
    // the current job this player has
    public ITask task;

    Rigidbody rigidBody;
    Player player;

    public delegate bool SendItemToWorld(Item i, Vector3Int pos,Inventory inv);
    public delegate bool RecvFromWorld(Item i, Vector3Int pos, Inventory inv);

    public SendItemToWorld SendHandle;
    public RecvFromWorld RecvHandle;




    // Start is called before the first frame update
    void Start()
    {



        rigidBody = GetComponent<Rigidbody>();
        player = GetComponentInChildren<Player>();

        /*
        var midgoal = new Vector3Int(3, 2, 3);
        var mid2= new Vector3Int(0, 2, 0);
        var goal = new Vector3Int(5, 2,7);
        var queue = new Queue<Vector3Int>();
        
        queue.Enqueue(midgoal);
        queue.Enqueue(mid2);
        queue.Enqueue(goal);
   



        task = new RouteTask(transform,Vector3Int.CeilToInt(transform.position),goal,queue);
         */



    }

/*

    // Update is called once per frame
    void Update()
    {

        if (task == null)
        {
            return;
        }
        if (task.isComplete() == true)
        {
            task = null;
            stopMove();
            Debug.Log("goal reached");
            return;
        }

        var currTask = task.getCurrSubTask();
        if ( currTask is RouteTask)
        {
            var currSub = currTask as RouteTask;
            if (currSub.process(transform.position) == true)
            {
                stopMove();
                return;
            }
            HandleMoveTask(currSub.currTarget() as MoveSubTask);
        }

            if (currTask is WorldPickUpTask)
            {
            HandleWorldPickUpTask(currTask as WorldPickUpTask);
            }

        
    }
    private void stopMove()
    {
        rigidBody.velocity = new Vector3();
    }

    private void HandleMoveTask(MoveSubTask t)
    {
        Debug.Log(t.Target);
        var target = t.Target;

        var dir = target - transform.position;
        dir=dir.normalized;

        rigidBody.velocity = player.speed*dir;

       

    }

    private void HandleWorldPickUpTask(WorldPickUpTask t)
    {
       
        if (t.Action == InteracitonType.SEND)
        {
            var test = SendHandle(t.TItem, t.Pos, player.Inv);
        }
        else
        {
            var test = RecvHandle(t.TItem, t.Pos, player.Inv);
            t.completed();
        }
    }
}

*/
