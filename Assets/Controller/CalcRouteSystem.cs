using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public enum RequestTaskState
{
    Untouched,
    Requested,
    Done
}


public enum RequestTaskVariant
{
    Pos,
    Item
}

// makes a request and waits for the answer
public struct RequestTaskComp : IComponentData
{
    public RequestTaskState state;
    public Entity target;

    //result
    public float3 pos;
    public Entity interactionObject;
    public RequestTaskVariant variant;




    public RequestTaskComp(Entity target, RequestTaskVariant variant)
    {
        this.target = target;
        this.state = RequestTaskState.Untouched;
        this.interactionObject = Entity.Null;
        this.pos = new float3();
        this.variant = variant;
    }

}

public class CalcRouteSystem : JobComponentSystem
{
    public BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {
        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    public struct RouteJob : IJobForEachWithEntity<Translation, RequestTaskComp>
    {

        public EntityCommandBuffer.Concurrent CommandBuffer;
        readonly public BufferFromEntity<PosRequest> posRequestsBuffer;
        readonly public BufferFromEntity<PosResponse> posResponseBuffer;


        public RouteJob(EntityCommandBuffer.Concurrent CommandBuffer, BufferFromEntity<PosRequest> posRequestsBuffer, BufferFromEntity<PosResponse> posResponseBuffer)
        {
            this.CommandBuffer = CommandBuffer;
            this.posRequestsBuffer = posRequestsBuffer;
            this.posResponseBuffer = posResponseBuffer;
        }
        public void Execute(Entity entity, int index, ref Translation pos, ref RequestTaskComp t)
        {
            if (t.state == RequestTaskState.Untouched)
            {
                var request = new PosRequest { requester = entity };
                posRequestsBuffer[t.target].Add(request);
                CommandBuffer.AddComponent(index, t.target, new PosRequestTag { });
                t.state = RequestTaskState.Requested;

            }
            //waiting for answer
            else if (t.state == RequestTaskState.Requested)
            {
                var responses = posResponseBuffer[entity];
                for (int i = 0; i < responses.Length; i++)
                {
                    if (responses[i].sender == t.target)
                    {
                        t.pos = responses[i].pos;
                        t.state = RequestTaskState.Done;
                        responses.RemoveAt(i);
                        return;
                    }
                }

            }
        }

    }
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        BufferFromEntity<PosRequest> posRequestsBuffer = GetBufferFromEntity<PosRequest>();
        var posResponseBuffer = GetBufferFromEntity<PosResponse>();
        ComponentDataFromEntity<MoveTask> moveTaskList = GetComponentDataFromEntity<MoveTask>();

        var job = new RouteJob(m_EntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent(), posRequestsBuffer, posResponseBuffer)
        .Schedule(this, inputDeps);

        return job;

    }
}