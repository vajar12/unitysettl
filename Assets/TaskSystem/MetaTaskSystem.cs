using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class MetaTaskSystem : JobComponentSystem
{

    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    //StorageSystem storageSys;

    // go through task steps
    // if one is finished go to next step
    public struct TaskJob : IJobForEachWithEntity<CarryTaski>
    {

        readonly ComponentDataFromEntity<Translation> positions;

        readonly ComponentDataFromEntity<MoveTask> moveTaskList;
        readonly ComponentDataFromEntity<InteractionTask> interactionTaskList;
        public EntityCommandBuffer.Concurrent CommandBuffer;

        public TaskJob(EntityCommandBuffer.Concurrent CommandBuffer, ComponentDataFromEntity<Translation> positions, ComponentDataFromEntity<MoveTask> moveTaskList, ComponentDataFromEntity<InteractionTask> interactionTaskList)
        {
            this.CommandBuffer = CommandBuffer;
            this.positions = positions;
            this.moveTaskList = moveTaskList;
            this.interactionTaskList = interactionTaskList;
        }
        public void Execute(Entity entity, int index, ref CarryTaski t)
        {
            if (t.state == CarryTaskStates.Untouched)
            {
                Translation pos;
                if (t.variant == CarryTaskVariant.ExtraPos)
                {
                    pos = new Translation { Value = t.targetPos };
                }
                else
                {
                    pos = positions[t.target];
                }
                var subTask = new MoveTask { target = pos.Value };
                CommandBuffer.AddComponent(index, entity, subTask);
                t.NextState();

            }
            else if (t.state == CarryTaskStates.Walk)
            {
                if (moveTaskList[entity].state == MoveTaskState.Done)
                {
                    CommandBuffer.RemoveComponent<MoveTask>(index, entity);
                    startInteraction(entity, index, ref t);
                }

            }
            else if (t.state == CarryTaskStates.Interact)
            {

                if (interactionTaskList[entity].done)
                {
                    CommandBuffer.RemoveComponent<InteractionTask>(index, entity);
                    t.NextState();
                    CommandBuffer.RemoveComponent<CarryTaski>(index, entity);
                }
            }
            else
            {
                Debug.Log("should never happen!!");
            }
        }

        private void startInteraction(Entity entity, int index, ref CarryTaski t)
        {
            var subTask = new InteractionTask(entity, t.target, t.interactionObject, InteracitonType.RECEIVE);
            CommandBuffer.AddComponent<InteractionTask>(index, entity, subTask);
            t.NextState();
        }
    }

    //:(
    public struct TakeJob : IJobForEachWithEntity<HaulTask>
    {

        readonly ComponentDataFromEntity<Translation> posList;
        readonly ComponentDataFromEntity<CarryTaski> carryTaskList;

        readonly ComponentDataFromEntity<InteractionTask> interactionTaskList;

        readonly ComponentDataFromEntity<RequestTaskComp> RequestTask;
        public EntityCommandBuffer.Concurrent CommandBuffer;

        public TakeJob(EntityCommandBuffer.Concurrent CommandBuffer, ComponentDataFromEntity<CarryTaski> carryTaskList, ComponentDataFromEntity<RequestTaskComp> routeTaskList,
         ComponentDataFromEntity<InteractionTask> interactionTaskList, ComponentDataFromEntity<Translation> posList)
        {
            this.CommandBuffer = CommandBuffer;
            this.carryTaskList = carryTaskList;
            this.RequestTask = routeTaskList;
            this.interactionTaskList = interactionTaskList;
            this.posList = posList;
        }
        public void Execute(Entity entity, int index, ref HaulTask t)
        {
            // cleaning and decision for state transition
            if (t.state == HaulTaskStates.Untouched)
            {

                NextState(entity, index, ref t);
                Debug.Log(t.objectSrc);

            }
            else if (t.state == HaulTaskStates.RequestItem)
            {
                if (RequestTask[entity].state == RequestTaskState.Done)
                {
                    var result = RequestTask[entity];
                    CommandBuffer.RemoveComponent<RequestTaskComp>(index, entity);
                    NextState(entity, index, ref t, result);
                }
            }
            else if (t.state == HaulTaskStates.TakeObject)
            {
                Debug.Log("state reached");
                if (carryTaskList[entity].state == CarryTaskStates.Done)
                {
                    CommandBuffer.RemoveComponent<CarryTaski>(index, entity);
                    NextState(entity, index, ref t);
                }

            }
            else if (t.state == HaulTaskStates.RequestPlace_BringObject)
            {


                if (RequestTask[entity].state == RequestTaskState.Done)
                {

                    var result = RequestTask[entity];
                    CommandBuffer.RemoveComponent<RequestTaskComp>(index, entity);
                    NextState(entity, index, ref t, result);
                }
            }
            else if (t.state == HaulTaskStates.BringObject)
            {
                Debug.Log("state reached");

                if (carryTaskList[entity].state == CarryTaskStates.Done)
                {
                    CommandBuffer.RemoveComponent<CarryTaski>(index, entity);
                    NextState(entity, index, ref t);
                }

            }
            else if (t.state == HaulTaskStates.Finished)
            {
                Debug.Log("done");
            }
            else
            {
                Debug.Log("should never happen!!");
            }




        }
        // opt1 is used to send position and item when needed
        public void NextState(Entity entity, int index, ref HaulTask t, RequestTaskComp opt1 = new RequestTaskComp())
        {
            t.NextState();
            if (t.state == HaulTaskStates.TakeObject)
            {
                CarryTaski subTask;

                if (t.variant == HaultTaskVariant.RequestItem)
                {
                    subTask = new CarryTaski(InteracitonType.RECEIVE, t.objectSrc, opt1.interactionObject, opt1.pos, CarryTaskVariant.ExtraPos);
                }
                else
                {
                    subTask = new CarryTaski(InteracitonType.RECEIVE, t.objectSrc, t.interactionObject, posList[t.interactionObject].Value, CarryTaskVariant.ExtraPos);
                }

                CommandBuffer.AddComponent(index, entity, subTask);
            }
            else if (t.state == HaulTaskStates.RequestItem)
            {
                var subTask = new RequestTaskComp(t.objectTarget, RequestTaskVariant.Item);
                CommandBuffer.AddComponent(index, entity, subTask);

            }
            else if (t.state == HaulTaskStates.RequestPlace_BringObject)
            {
                var subTask = new RequestTaskComp(t.objectTarget, RequestTaskVariant.Pos);
                CommandBuffer.AddComponent(index, entity, subTask);
            }
            else if (t.state == HaulTaskStates.BringObject)
            {
                // just walk to blueprint
                if (t.variant == HaultTaskVariant.RequestItem)
                {
                    var subTask = new CarryTaski(InteracitonType.SEND, t.objectSrc, t.objectTarget, posList[t.objectTarget].Value, CarryTaskVariant.ExtraPos);
                    CommandBuffer.AddComponent(index, entity, subTask);
                }
                else
                {
                    var subTask = new CarryTaski(InteracitonType.RECEIVE, t.objectTarget, t.interactionObject, opt1.pos, CarryTaskVariant.ExtraPos);
                    CommandBuffer.AddComponent(index, entity, subTask);
                }

            }

        }

    }


    protected override void OnCreate()
    {
        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();


    }

    public float deltaTime = 1;

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {


        var positions = GetComponentDataFromEntity<Translation>();
        var moveTaskList = GetComponentDataFromEntity<MoveTask>();
        var interactionList = GetComponentDataFromEntity<InteractionTask>();
        var carryTaskList = GetComponentDataFromEntity<CarryTaski>();
        var routeTaskList = GetComponentDataFromEntity<RequestTaskComp>();


        var cmBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent();

        var taskJob = new TaskJob(cmBuffer, positions, moveTaskList, interactionList).Schedule(this, inputDeps);
        m_EntityCommandBufferSystem.AddJobHandleForProducer(taskJob);

        var takeJob = new TakeJob(cmBuffer, carryTaskList, routeTaskList, interactionList, positions).Schedule(this, taskJob);
        m_EntityCommandBufferSystem.AddJobHandleForProducer(takeJob);

        return takeJob;

    }
}
