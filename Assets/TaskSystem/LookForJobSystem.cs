using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class LookForJobSystem : ComponentSystem
{

    StorageSystem storageSys;



    int playerChunkindex = -1;



    protected override void OnCreate()
    {
        storageSys = World.GetOrCreateSystem<StorageSystem>();

    }








    // first version
    // look for closest item and take to storage
    // haultask

    protected override void OnUpdate()
    {
        var haulTaskQ = GetEntityQuery(typeof(OpenHaulTask)).ToComponentDataArray<OpenHaulTask>(Allocator.TempJob);
        var haulTaskE = GetEntityQuery(typeof(OpenHaulTask)).ToEntityArray(Allocator.TempJob);
        var buildTaskQ = GetEntityQuery(typeof(OpenBuildTask)).ToComponentDataArray<OpenBuildTask>(Allocator.TempJob);
        var buildTaskE = GetEntityQuery(typeof(OpenBuildTask)).ToEntityArray(Allocator.TempJob);


        var players = GetEntityQuery(ComponentType.ReadOnly<PlayerTag>(), ComponentType.ReadOnly<IdleTag>()).ToEntityArray(Allocator.TempJob);

        var StorageQuery = GetEntityQuery(ComponentType.ReadOnly<StorageComp>(), ComponentType.ReadOnly<Translation>());
        var storages = StorageQuery.ToEntityArray(Allocator.TempJob);
        var storagesLoc = StorageQuery.ToComponentDataArray<Translation>(Allocator.TempJob);



        var positions = GetComponentDataFromEntity<Translation>();

        for (int p = 0; p < players.Length; p++)
        {
            if (buildTaskQ.Length > 0)
            {
                Debug.Log("test");
                var newTask = buildTaskQ[0];
                var closestStorage = FindClosestStorageWithResource(newTask.type, 1, storages);
                var data = HaulTask.HaultTaskItemReq(closestStorage, newTask.blueprint);
                PostUpdateCommands.AddComponent(players[p], data);
                PostUpdateCommands.RemoveComponent<IdleTag>(players[p]);
                PostUpdateCommands.DestroyEntity(buildTaskE[0]);

            }

            else if (haulTaskQ.Length > 0)
            {
                Debug.Log("test");
                var newTask = haulTaskQ[0];
                var closestStorage = FindClosestStorage(positions[newTask.item], storagesLoc);
                var data = new HaulTask(newTask.item, storages[closestStorage], newTask.item);
                PostUpdateCommands.AddComponent(players[p], data);
                PostUpdateCommands.RemoveComponent(players[p], typeof(IdleTag));
                PostUpdateCommands.DestroyEntity(haulTaskE[0]);
            }


        }
        haulTaskQ.Dispose();
        buildTaskQ.Dispose();
        haulTaskE.Dispose();
        buildTaskE.Dispose();
        storagesLoc.Dispose();
        storages.Dispose();
        players.Dispose();

    }

    //super inefficent, query every storage if they contauin the desired item...
    private Entity FindClosestStorageWithResource(ResourceType type, int quantity, NativeArray<Entity> storages)
    {

        return storages[0];

    }
    private int FindClosestStorage(Translation obj, NativeArray<Translation> storagesLoc)
    {
        var closestDist = float.MaxValue;
        var ctr = -1;
        for (int i = 0; i < storagesLoc.Length; i++)
        {
            if (math.distance(obj.Value, storagesLoc[i].Value) < closestDist)
            {
                closestDist = math.distance(obj.Value, storagesLoc[i].Value);
                ctr = i;
            }

        }


        return ctr;


    }
}