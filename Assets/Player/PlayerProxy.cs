using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;

[RequiresEntityConversion]
public class PlayerProxy : MonoBehaviour, IConvertGameObjectToEntity
{

    public float speed = 1;
    public float rotSpeed = 2;


    public RenderMesh mesh;

    /*
        private void Start(){

            var manager=World.Active.EntityManager;

            var archtype=manager.CreateArchetype(typeof(SpeedComponent),typeof(Translation),typeof(RenderMesh),typeof(LocalToWorld));

            var arr=new NativeArray<Entity>(1,Allocator.Temp);

            manager.CreateEntity(archtype,arr);

            for(int i=0;i<arr.Length;i++){
                Entity ent=arr[i];

                manager.SetSharedComponentData(ent,mesh);

            }

        }
        */


    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {

        Debug.Log("test2");
        var data = new SpeedComponent { speed = speed };
        dstManager.AddComponentData(entity, data);


        dstManager.AddSharedComponentData(entity, mesh);
        dstManager.AddComponentData(entity, new PlayerTag { });
        dstManager.AddComponentData(entity, new IdleTag { });
        dstManager.AddComponentData(entity, new InventoryComponent { maxSize = 5, weight = 100 });
        dstManager.AddBuffer<ItemComp>(entity);
        dstManager.AddBuffer<PosResponse>(entity);
        dstManager.AddComponentData(entity, new Scale { Value = 2 });
        dstManager.AddComponentData(entity, new ScalePivot { Value = new float3(5, 2, 2) });

        //var test = dstManager.GetBuffer<ItemComp>(entity);
        //Debug.Log(test.Length);
    }





}

public struct PlayerTag : IComponentData
{

}
public struct IdleTag : IComponentData
{

}



