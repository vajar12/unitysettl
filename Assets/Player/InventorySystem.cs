using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;


public class InventorySystem : JobComponentSystem
{

    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {


        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();


    }

    public struct ReceiveSendJob : IJobForEachWithEntity<InventoryComponent, InteractionNotification>
    {
        [ReadOnly] public BufferFromEntity<ItemComp> inventory;
        public EntityCommandBuffer.Concurrent CommandBuffer;
        public void Execute(Entity entity, int index, ref InventoryComponent c0, ref InteractionNotification noti)
        {
            Debug.Log(entity);
            CommandBuffer.RemoveComponent(index, entity, typeof(InteractionNotification));
            var inv = inventory[entity];
            if (noti.type == InteracitonType.RECEIVE)
            {

                for (int i = 0; i < inv.Length; i++)
                {
                    if (inv[i].e == Entity.Null)
                    {
                        Debug.Log(i);
                        var invComp = new ItemComp { e = noti.interactionObject };
                        inv.Insert(i, invComp);
                        return;

                    }

                }

                if (inv.Length < c0.maxSize)
                {
                    inv.Add(new ItemComp { e = noti.interactionObject });
                    return;
                }
                Debug.Log("jo bad");
            }
            if (noti.type == InteracitonType.SEND)
            {

                for (int i = 0; i < inv.Length; i++)
                {
                    if (inv[i].e == noti.interactionObject)
                    {
                        inv.RemoveAt(i);
                        return;
                    }

                }
            }

        }
    }






    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var job = new ReceiveSendJob
        {
            inventory = GetBufferFromEntity<ItemComp>(),
            CommandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent(),
        }.Schedule(this, inputDeps);

        m_EntityCommandBufferSystem.AddJobHandleForProducer(job);
        return job;
    }
}