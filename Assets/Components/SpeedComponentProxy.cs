﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

[RequiresEntityConversion]
public class SpeedComponentProxy : MonoBehaviour, IConvertGameObjectToEntity {

    public float speed = 1;

    public void Convert (Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        var data = new SpeedComponent { speed = speed };
        dstManager.AddComponentData (entity, data);
    }
}