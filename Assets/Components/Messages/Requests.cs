using Unity.Entities;
using Unity.Mathematics;

public struct PosRequest : IBufferElementData
{

    public Entity requester;


}

public struct ItemRequest : IBufferElementData
{
    public ResourceType type;
    public int quantity;
}

public struct ItemResponse : IBufferElementData
{
    public Entity item;
    public float3 pos;
    public Entity sender;
    public int corrId;
}


public struct ItemRequestTag : IComponentData
{ }
public struct ItemResponseTag : IComponentData
{ }
public struct PosRequestTag : IComponentData
{ }


public struct PosResponse : IBufferElementData
{
    public bool success;
    public float3 pos;
    public Entity sender;
}

public struct PosResponseTag : IComponentData
{
}