using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

/* 
class TerrainSystem : JobComponentSystem
{
    public int worldSizeX = WorldDefinitions.worldSize.x;
    public int worldSizeY = WorldDefinitions.worldSize.y;
    public int worldSizeZ = WorldDefinitions.worldSize.z;
    public EntityArchetype block;

    public RenderMesh mesh;
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {
        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();


        block = World.Active.EntityManager.CreateArchetype(typeof(Translation), typeof(RenderMesh), typeof(LocalToWorld));


    }

    struct genJob : IJobForEachWithEntity<createJobComp>
    {


        public int worldSizeX;
        public int worldSizeY;
        public int worldSizeZ;






        public EntityCommandBuffer.Concurrent CommandBuffer;


        public void Execute(Entity entity, int index, ref createJobComp j)
        {

            Debug.Log("jo");

            for (int x = 0; x < worldSizeX; x++)
            {
                for (int y = 0; y < worldSizeY; y++)
                {
                    for (int z = 0; z < worldSizeZ; z++)
                    {
                        if (y == 0)
                        {

                            var ent = CommandBuffer.Instantiate(index, j.ent);

                            var p = j.pos;
                            p.x += x;
                            p.z += z;

                            var loc = new Translation { Value = p };
                            CommandBuffer.SetComponent(index, ent, loc);

                        }
                        else
                        {

                        }
                    }
                }
            }

            CommandBuffer.DestroyEntity(index, entity);
        }
    }




    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {


        return new genJob
        {
            CommandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent(),
            worldSizeX = worldSizeX,
            worldSizeY = worldSizeY,
            worldSizeZ = worldSizeZ,
        }
        .Schedule(this, inputDeps);


    }
}*/