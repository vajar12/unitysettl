using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Unity.Transforms;

public class InteractionSystem : JobComponentSystem
{

    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    BufferFromEntity<ItemComp> items;



    EntityArchetype invNot;



    public struct InteractJob : IJobForEachWithEntity<InteractionTask>
    {
        public EntityCommandBuffer.Concurrent CommandBuffer;
        public readonly BufferFromEntity<ItemComp> items;

        public readonly ComponentDataFromEntity<Translation> positions;

        public EntityArchetype invNot;



        public InteractJob(BufferFromEntity<ItemComp> items, ComponentDataFromEntity<Translation> positions, EntityArchetype invNot, EntityCommandBuffer.Concurrent CommandBuffer)
        {
            this.items = items;
            this.positions = positions;
            this.invNot = invNot;
            this.CommandBuffer = CommandBuffer;

        }

        // check if in range
        // notify inventory that an item will be added
        // notify world that item will be taken

        // next round check 
        public void Execute(Entity entity, int index, ref InteractionTask task)
        {

            if (Unity.Mathematics.math.distance(positions[task.src].Value, positions[task.src].Value) > 1)
            {

                // no valid interaction possible
                //todo longrange stuff maybe
                // todo propagate error
                CommandBuffer.RemoveComponent<InteractionTask>(index, entity);
                return;
            }

            if (task.type == InteracitonType.RECEIVE)
            {
                CommandBuffer.AddComponent(index, task.src, new InteractionNotification { type = InteracitonType.RECEIVE, interactionObject = task.InteractionObject, counterpart = task.target });
                CommandBuffer.AddComponent(index, task.target, new InteractionNotification { type = InteracitonType.SEND, interactionObject = task.InteractionObject, counterpart = task.src });
                task.done = true;
            }
            else if (task.type == InteracitonType.SEND)
            {
                CommandBuffer.AddComponent(index, task.src, new InteractionNotification { type = InteracitonType.SEND, interactionObject = task.InteractionObject, counterpart = task.target });
                CommandBuffer.AddComponent(index, task.target, new InteractionNotification { type = InteracitonType.RECEIVE, interactionObject = task.InteractionObject, counterpart = task.src });
                task.done = true;
            }
        }
    }


    protected override void OnCreate()
    {

        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();

        invNot = World.Active.EntityManager.CreateArchetype(typeof(InteractionNotification));
    }





    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        var items = GetBufferFromEntity<ItemComp>(true);

        var positions = GetComponentDataFromEntity<Translation>();

        var job = new InteractJob(
            items,
            positions,
            invNot,
            m_EntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent()
        );
        return job.Schedule(this, inputDeps);
    }
}