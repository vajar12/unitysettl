using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;



//todo tag item if in world



public class WSystem : JobComponentSystem
{


     BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {
        Debug.Log("hallol");

        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();


    }


    [RequireComponentTag(typeof(ItemWorldTag))]
    public struct InteractJob : IJobForEachWithEntity<InteractionNotification>
    {
        public EntityCommandBuffer.Concurrent CommandBuffer;

        public void Execute(Entity entity, int index, ref InteractionNotification noti)
        {
            CommandBuffer.RemoveComponent<InteractionNotification>(index,entity);

            
            if (noti.type == InteracitonType.SEND)
            {
                Debug.Log("tesssststt");
                CommandBuffer.RemoveComponent<RenderMesh>(index, entity);
            }
        }
    }


   
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {


        var job= new InteractJob
        {
            CommandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent(),
        }.Schedule(this, inputDeps);

        m_EntityCommandBufferSystem.AddJobHandleForProducer(job);
        return job;
    }

}