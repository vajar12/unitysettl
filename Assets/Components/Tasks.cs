﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[Serializable]
public struct MoveTask : IComponentData
{
    public float3 target;
    public MoveTaskState state;

    public MoveTask(float3 target)
    {
        this.target = target;
        this.state = MoveTaskState.Untouched;
    }



}

public enum MoveTaskState
{
    Untouched,
    Done
}

public struct InteractionTask : IComponentData
{
    public Entity src;
    public Entity target;

    public Entity InteractionObject;

    public InteracitonType type;

    public bool done;

    public InteractionTask(Entity src, Entity target, Entity InteractionObject, InteracitonType type)
    {
        this.src = src;
        this.target = target;
        this.InteractionObject = InteractionObject;
        this.type = type;
        this.done = false;

    }

}

public enum HaultTaskVariant
{
    RequestTargetPos,
    RequestItem,
    Default
}

// take item from one place to another, maybe request stuff
public struct HaulTask : IComponentData
{
    public HaulTaskStates state;
    public Entity objectSrc;
    public Entity objectTarget;
    public Entity interactionObject;

    public HaultTaskVariant variant;


    public HaulTask(Entity objectSrc, Entity objectTarget, Entity InteractionObject, HaultTaskVariant variant = HaultTaskVariant.Default)
    {
        this.objectSrc = objectSrc;
        this.objectTarget = objectTarget;
        this.interactionObject = InteractionObject;
        this.state = HaulTaskStates.Untouched;
        this.variant = variant;

    }

    public static HaulTask HaultTaskItemReq(Entity objectSrc, Entity objectTarget)
    {
        return new HaulTask
        {
            state = HaulTaskStates.Untouched,
            objectSrc = objectSrc,
            objectTarget = objectTarget,
            variant = HaultTaskVariant.RequestItem
        };
    }

    public void NextState()
    {
        switch (state)
        {
            case HaulTaskStates.Untouched:
                if (variant == HaultTaskVariant.RequestItem)
                {
                    state = HaulTaskStates.RequestItem;

                }
                else
                {
                    state = HaulTaskStates.TakeObject;
                }
                break;
            case HaulTaskStates.RequestItem:
                state = HaulTaskStates.TakeObject;
                break;
            case HaulTaskStates.TakeObject:
                state = HaulTaskStates.RequestPlace_BringObject;
                break;
            case HaulTaskStates.RequestPlace_BringObject:
                state = HaulTaskStates.BringObject;
                break;
            case HaulTaskStates.BringObject:
                state = HaulTaskStates.Finished;
                break;
        }


    }

}

public enum CarryTaskVariant
{
    ItemPos,
    ExtraPos
}


public struct CarryTaski : IComponentData
{

    public InteracitonType type;
    public Entity target;
    public Entity interactionObject;

    public float3 targetPos;

    public CarryTaskStates state;

    public CarryTaskVariant variant;




    public CarryTaski(InteracitonType type, Entity target, Entity InteractionObject, float3 targetPos, CarryTaskVariant variant)
    {
        this.type = type;
        this.target = target;
        this.interactionObject = InteractionObject;
        this.state = CarryTaskStates.Untouched;
        this.targetPos = targetPos;
        this.variant = variant;
    }



    public void NextState()
    {
        switch (state)
        {
            case CarryTaskStates.Untouched:
                state = CarryTaskStates.Walk;
                break;
            case CarryTaskStates.Walk:
                state = CarryTaskStates.Interact;
                break;
            case CarryTaskStates.Interact:
                state = CarryTaskStates.Done;
                break;

        }
    }

}

public enum CarryTaskStates
{
    Untouched,
    Walk,
    Interact,
    Done
}

public enum HaulTaskStates
{
    Untouched,
    RequestItem,
    TakeObject,
    RequestPlace_BringObject,
    BringObject,
    Finished
}