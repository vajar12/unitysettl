using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

[RequiresEntityConversion]
class genTerrainProxy : MonoBehaviour, IConvertGameObjectToEntity {

    public RenderMesh mesh;

    private void Update () {

    }

    public void Convert (Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        var pos = new float3 (transform.position.x, transform.position.y, transform.position.z);
        var jobC = new createJobComp { ent = entity, pos = pos };

        //dstManager.AddSharedComponentData(entity, mesh);

        var job = dstManager.CreateEntity ();
        dstManager.AddComponentData (job, jobC);
    }

}

//tag
public struct createJobComp : IComponentData {
    // unique style to use    
    public Entity ent;
    public float3 pos;

}