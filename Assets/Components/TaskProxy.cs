﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

[RequiresEntityConversion]
public class TaskProxy : MonoBehaviour, IConvertGameObjectToEntity
{
    //public float3 target;

    public RenderMesh mesh;

    struct ItemTag : IComponentData
    {
        public float a;
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {

        var tItem = dstManager.CreateEntity(typeof(RenderMesh), typeof(Translation), typeof(LocalToWorld), typeof(Scale), typeof(RenderBounds));
        dstManager.SetSharedComponentData(tItem, mesh);

        dstManager.AddComponentData(tItem, new ItemTag { a = 4 });
        dstManager.AddComponentData(tItem, new ItemWorldTag { });
        //var data = new CarryTaski(ItemAction.PICKUP, tItem,tItem);
        //dstManager.AddComponentData(entity, data);
        dstManager.SetComponentData(tItem, new Translation { Value = new float3(5, 0, 5) });
        dstManager.SetComponentData(tItem, new Scale { Value = 0.5f });

        var haulT = dstManager.CreateEntity(typeof(OpenHaulTask));
        dstManager.SetComponentData(haulT, new OpenHaulTask { item = tItem });


    }
}